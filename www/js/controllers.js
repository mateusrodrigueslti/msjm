angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope, $cordovaSocialSharing) {
  $scope.eventos = [
    { data: 'Dia 01/06', id: 1 },
    { data: 'Dia 02/06', id: 2 },
    { data: 'Dia 03/06', id: 3 },
    { data: 'Dia 04/06', id: 4 },
    { data: 'Dia 05/06', id: 5 },
    { data: 'Dia 06/06', id: 6 },
    { data: 'Dia 07/06', id: 7 },
    { data: 'Dia 08/06', id: 8 },
    { data: 'Dia 09/06', id: 9 },
    { data: 'Dia 10/06', id: 10 },
    { data: 'Dia 11/06', id: 11 },
    { data: 'Dia 12/06', id: 12 },
    { data: 'Dia 13/06', id: 13 },
    { data: 'Dia 14/06', id: 14 },
    { data: 'Dia 15/06', id: 15 },
    { data: 'Dia 16/06', id: 16 },
    { data: 'Dia 17/06', id: 17 },
    { data: 'Dia 18/06', id: 18 },
    { data: 'Dia 19/06', id: 19 },
    { data: 'Dia 20/06', id: 20 },
    { data: 'Dia 21/06', id: 21 },
    { data: 'Dia 22/06', id: 22 },
    { data: 'Dia 23/06', id: 23 },
    { data: 'Dia 24/06', id: 24 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
